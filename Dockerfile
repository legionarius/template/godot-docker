FROM ubuntu:groovy
LABEL author="https://github.com/aBARICHELLO/godot-ci/graphs/contributors"

USER root
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    git \
    git-lfs \
    python3 \
    python3-openssl \
    unzip \
    wget \
    zip \
    adb \
    openjdk-8-jdk-headless \
    clang-format \
    g++ \
    scons \
    build-essential \
    pkg-config \
    libx11-dev \
    libxcursor-dev \
    libxinerama-dev \
    libgl1-mesa-dev \
    libglu-dev \
    libasound2-dev \
    libpulse-dev \
    libudev-dev \ 
    libxi-dev \ 
    libxrandr-dev \
    yasm \
    mingw-w64 \
    clang \
    && rm -rf /var/lib/apt/lists/*

ENV GODOT_VERSION "3.3.2"

RUN wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
    && wget https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
    && mkdir ~/.cache \
    && mkdir -p ~/.config/godot \
    && mkdir -p ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
    && unzip Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip \
    && mv Godot_v${GODOT_VERSION}-stable_linux_headless.64 /usr/local/bin/godot \
    && unzip Godot_v${GODOT_VERSION}-stable_export_templates.tpz \
    && mv templates/* ~/.local/share/godot/templates/${GODOT_VERSION}.stable \
    && rm -f Godot_v${GODOT_VERSION}-stable_export_templates.tpz Godot_v${GODOT_VERSION}-stable_linux_headless.64.zip


RUN git clone https://github.com/emscripten-core/emsdk.git /opt/emsdk \
    && cd /opt/emsdk \
    && ./emsdk install latest \
    && ./emsdk activate latest
    
ENV PATH = "/opt/emsdk:/opt/emsdk/upstream/emscripten:/opt/emsdk/node/14.15.5_64bit/bin:${PATH}" EMSDK = "/opt/emsdk" EM_CONFIG = "/opt/emsdk/.emscripten" EM_CACHE = "/opt/emsdk/upstream/emscripten/cache" EMSDK_NODE = "/opt/emsdk/node/14.15.5_64bit/bin/node"

ADD getbutler.sh /opt/butler/getbutler.sh
RUN bash /opt/butler/getbutler.sh
RUN /opt/butler/bin/butler -V

ENV PATH="/opt/butler/bin:${PATH}"

# Setup Android NDK

RUN wget https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip
RUN mkdir -p ~/Android/Sdk/cmdline-tools
RUN unzip commandlinetools-linux-7302050_latest.zip -d ~/Android/Sdk/cmdline-tools
RUN mv ~/Android/Sdk/cmdline-tools/cmdline-tools ~/Android/Sdk/cmdline-tools/latest

ENV PATH="~/Android/Sdk/cmdline-tools/latest/bin:${PATH}"

RUN yes | ~/Android/Sdk/cmdline-tools/latest/bin/sdkmanager --licenses
RUN ~/Android/Sdk/cmdline-tools/latest/bin/sdkmanager --install "ndk;22.1.7171670" "build-tools;30.0.3"

ENV GODOT_CPP="/opt/godot-cpp"
ENV ANDROID_NDK_ROOT="/root/Android/Sdk/ndk/22.1.7171670"

# Adding android keystore and settings
RUN keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999 \
    && mv debug.keystore /root/debug.keystore
RUN godot -e -q
RUN echo 'export/android/adb = "/usr/bin/adb"' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/jarsigner = "/usr/bin/jarsigner"' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/debug_keystore = "/root/debug.keystore"' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/debug_keystore_user = "androiddebugkey"' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/debug_keystore_pass = "android"' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/force_system_user = false' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/timestamping_authority_url = ""' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/shutdown_adb_on_exit = true' >> ~/.config/godot/editor_settings-3.tres
RUN echo 'export/android/android_sdk_path = "/root/Android/Sdk"' >> ~/.config/godot/editor_settings-3.tres

RUN git clone --recursive https://github.com/godotengine/godot-cpp.git /opt/godot-cpp

COPY api.json /opt/godot-cpp/

WORKDIR /opt/godot-cpp

# Building...
RUN scons platform=linux target=release generate_bindings=yes bits=64 use_custom_api_file=yes custom_api_file=api.json \
&& scons platform=windows target=release bits=64 use_custom_api_file=yes custom_api_file=api.json \
&& scons platform=javascript target=release use_custom_api_file=yes custom_api_file=api.json \
&& scons platform=android target=release use_custom_api_file=yes custom_api_file=api.json \
&& scons platform=android target=release use_custom_api_file=yes custom_api_file=api.json android_arch=arm64v8
